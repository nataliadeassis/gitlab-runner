FROM debian:stable-slim as builder

# Definição do diretório de trabalho
WORKDIR /tools

# Atualiza e instala os pacotes básicos, dependências para Python, pacote python3-venv, AWS CLI, kubectl, Helm, Krew, df-pv usando Krew e limpa o cache do apt e remove pacotes não necessários
RUN apt update && \
    apt-get upgrade -y && \
    apt-get install -y curl git jq sshpass telnet iputils-ping unzip wget vim nano python3-dev libkrb5-dev python3 python3-pip python3-venv && \
    python3 -m venv /venv && \
    /venv/bin/pip install pywinrm requests-kerberos pexpect && \
    curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip" && \
    unzip awscliv2.zip && \
    ./aws/install && \
    curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl" && \
    install -o root -g root -m 0755 kubectl /usr/local/bin/kubectl && \
    rm kubectl && \
    curl https://raw.githubusercontent.com/helm/helm/master/scripts/get-helm-3 | bash && \
    set -x; cd "$(mktemp -d)" && \
    OS="$(uname | tr '[:upper:]' '[:lower:]')" && \
    ARCH="$(uname -m | sed -e 's/x86_64/amd64/' -e 's/\(arm\)\(64\)\?.*/\1\2/' -e 's/aarch64$/arm64/')" && \
    KREW="krew-${OS}_${ARCH}" && \
    curl -fsSLO "https://github.com/kubernetes-sigs/krew/releases/latest/download/${KREW}.tar.gz" && \
    tar zxvf "${KREW}.tar.gz" && \
    ./"${KREW}" install krew && \
    export PATH="${KREW_ROOT:-$HOME/.krew}/bin:$PATH" && \
    kubectl krew install df-pv && \
    apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# Copie o script 'cco.py' para o diretório de trabalho
COPY cco.py .

# Defina o comando padrão
CMD ["bash"]

# Stage final
FROM debian:stable-slim

COPY --from=builder /tools /tools

WORKDIR /tools

CMD ["bash"]
